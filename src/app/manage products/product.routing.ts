// Layouts
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {AddProductComponent} from "./add-product/add-product.component";
import { ListProductComponent } from "./list-product/list-product.component";
import { EditProductComponent } from "./edit-product/edit-product.component";

export const ProductRouting: Routes = [

  {
    path: 'add-product',
    component: AddProductComponent
  },
  {
    path: 'list-product',
    component: ListProductComponent
  },
  {
    path: 'edit-product/:id',
    component: EditProductComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(ProductRouting)],
  exports: [RouterModule]
})
export class ProductRoutingModule {
}
