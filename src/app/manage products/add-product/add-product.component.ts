import { Component, OnInit, ɵisListLikeIterable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { ProductService } from '../../shared/services/product.service';
import swal from 'sweetalert';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  form: FormGroup;
  isLinear = false;
  files: any;
  constructor(private _formBuilder: FormBuilder,
    private productService: ProductService, private route : Router) { }

  ngOnInit() {

    this.form = this._formBuilder.group({
      label: ['', Validators.required],
      prix: ['', Validators.required],
      quantite: ['', Validators.required],

    });
  }

   addProduct() {
    swal({
      title: "Succés",
      text: "Produit a été ajouté",
      icon: "success",
    });
    this.isLinear = true;
    const label = this.form.get('label').value;
    const prix = this.form.get('prix').value;
    const quantite = this.form.get('quantite').value;

    const product = {
      label: label,
      prix: prix,
      quantite: quantite
    };
  
    this.productService.addProduct(product).subscribe()
this.productService.getAll().subscribe();
    this.route.navigate(['/product/list-product']);
  }

  imageUpload(event){
    this.files=event.target.files[0];
    console.log(this.files);
  }
 

}
