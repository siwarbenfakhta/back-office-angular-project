import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/services/product.service';
import { Product } from '../../shared/models/product';
import swal from 'sweetalert';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
tableau:Product[]=[];
  constructor(private productService : ProductService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    this.productService.getAll().subscribe(res =>{
      this.tableau = res ;
    })

  }

  deleteProduct(id){
    swal({
      title: "Succés",
      text: "Produit a été supprimé",
      icon: "success",
    });
  this.productService.deleteProduct(id).subscribe(
    res => {
      this.getAll();
    }
  );

  }

}
