import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {ProductRoutingModule} from "./product.routing";
import {AddProductComponent} from "./add-product/add-product.component";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule} from '@angular/material/form-field';
import { ListProductComponent } from './list-product/list-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';

@NgModule({
  imports: [
    
    SharedModule,ProductRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,

    CommonModule
  ],
  declarations: [AddProductComponent, ListProductComponent, EditProductComponent]
})
export class ProductModule {
}
