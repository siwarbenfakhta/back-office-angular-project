import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../shared/services/product.service';
import { Product } from '../../shared/models/product';
@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  id: any;
  product = new Product;
  constructor(private route: ActivatedRoute, private productService: ProductService,private router : Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.getProductById();
  }

  getProductById() {
    this.productService.getProductbyId(this.id).subscribe(res => {
      this.product = res;
    })

  }
updateProduct(){
  this.productService.updateProduct(this.id , this.product).subscribe(res =>{
    console.log(res);
    this.router.navigate(['product/list-product'])
    console.log("done");
  })
}

}
