import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FullLayoutComponent} from './full-layout/full-layout.component';
import {LoginComponent} from './login/login.component';
import {ExampleModule} from "./example/example.module";
import {ProductModule} from "./manage products/product.module";
import { UserModule } from './manage users/users.module';

export function loadExampleModule() {
  return ExampleModule;
}
export function loadProductModule() {
  return ProductModule;
}
export function loadUserModule() {
  return UserModule;
}

export const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent
  },
  
  {
    path: '',
    component: FullLayoutComponent,
    children: [
      {
        path: 'example',
        loadChildren: loadExampleModule
      },
      {
        path: 'product',
        loadChildren: loadProductModule
      },
      {
        path:'users',
        loadChildren: loadUserModule

      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
