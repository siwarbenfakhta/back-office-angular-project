export class Product {
    id : number ;
    label : string;
    prix : number;
    quantite : number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}