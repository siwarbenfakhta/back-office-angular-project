export class Config {
  public static baseUrl = 'http://localhost/back-office-backend/public';
  public static adminUrl = Config.baseUrl + '/admin';
  public static product = Config.baseUrl + '/product';
  //public static addProduct = Config.baseUrl + '/addProduct';
  public static tokenKey = "token-backoffice";
  public static adminKey = "admin-key";
}
