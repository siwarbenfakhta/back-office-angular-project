import {Injectable} from "@angular/core";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {GenericService} from "./generic.service";
import {HttpClient} from "@angular/common/http";
import {Config} from "../config";
import {StorageService} from "./storage.service";
import {Product} from "../models/product";
import {Credentials} from "../models/credential";

@Injectable()
export class ProductService extends GenericService {
  
  constructor(private http: HttpClient, private storageService: StorageService) {
    super();

  }

  getAll() {
    
    return this.http.get<Product[]>(Config.product);
  }

  addProduct(product){
    
    return this.http.post<Product>(Config.baseUrl + "/addProduct" , product);
  }

  deleteProduct(id){
    return this.http.delete(Config.baseUrl + "/deleteProduct/" + id )
  }


  getProductbyId(id){
    return this.http.get<Product>(Config.baseUrl + "/getbyid/" + id )
  }

  updateProduct(id , product){
    
    return this.http.patch<Product>(Config.baseUrl + "/updateProduct/" + id, product);
  }


 

}
