import {Injectable} from "@angular/core";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {GenericService} from "./generic.service";
import {HttpClient} from "@angular/common/http";
import {Config} from "../config";
import {StorageService} from "./storage.service";
import {User} from "../models/User";
import {Credentials} from "../models/credential";

@Injectable()
export class UserService extends GenericService {
  
  constructor(private http: HttpClient, private storageService: StorageService) {
    super();

  }

  getAll() {
    
    return this.http.get<User[]>(Config.baseUrl + '/users');
  }

  deleteUser(id){
    return this.http.delete(Config.baseUrl + "/deleteUser/" + id )
  }



 

}
