import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { User } from '../../shared/models/user';
import swal from 'sweetalert';
@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  tableau:User[]=[];
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    this.userService.getAll().subscribe(res =>{
      this.tableau = res ;
    })

  }

  deleteUser(id){
    swal({
      title: "Succés",
      text: "Produit a été supprimé",
      icon: "success",
    });
  this.userService.deleteUser(id).subscribe(
    res => {
      this.getAll();
    }
  );

  }

}
