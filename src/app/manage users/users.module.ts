import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule} from '@angular/material/form-field';
import { ListUsersComponent } from './list-users/list-users.component';
import { UserRoutingModule } from './users.routing';

@NgModule({
  imports: [
    
    SharedModule,UserRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,

    CommonModule
  ],
  declarations: [ListUsersComponent]
})
export class UserModule {
}
