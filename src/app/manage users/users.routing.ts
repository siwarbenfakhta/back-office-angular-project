// Layouts
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import { ListUsersComponent } from "./list-users/list-users.component";


export const ProductRouting: Routes = [

  {
    path: 'listusers',
    component: ListUsersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(ProductRouting)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
